#include<iostream>
#include<SFML\Graphics.hpp>
#include"Game.h"

const int SCREEN_WIDTH(1920);
const int SCREEN_HEIGHT(1080);

int main()
{
    Game game(SCREEN_WIDTH, SCREEN_HEIGHT); 
    game.run();
    return 0;
}
