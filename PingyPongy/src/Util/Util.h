#pragma once
#include<SFML\Graphics.hpp>
#include"..\Entities\Entity.h"

class Util
{
public:
    static float getPointToMouseAngle(Entity &obj, sf::Vector2i &mouseCordinates)
    {
        sf::Vector2f objPosition = obj.m_Position;
        sf::Vector2f objSize = obj.m_Size;

        double PI = 3.14;
        int deltaX = (int)mouseCordinates.x - (int)objPosition.x - (int)(objSize.y / 2);
        int deltaY = (int)mouseCordinates.y - (int)objPosition.y - (int)objSize.y * 2;

        double angleInDegrees = atan2(deltaY, deltaX) * (180 / PI);
        return (float)angleInDegrees;
    }

    static float rotateTowardsObject(Entity &objA, Entity &objB)
    {
       // m_Position = m_HookSprite.getPosition();
        //sf::Vector2f cannonPosition = m_HookCannon->m_CannonSprite.getPosition();
        double PI = 3.14;
        int deltaX = (int)(objB.m_Position.x + objB.m_Size.x - 6) - (int)objA.m_Position.x; //- (m_Size.y / 2);
        int deltaY = (int)objB.m_Position.y - (int)objA.m_Position.y;// - m_Size.y * 2;

        double angleInDegrees = atan2(deltaY, deltaX) * (180 / PI);
        return (float)angleInDegrees;
    }
};