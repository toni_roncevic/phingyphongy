#pragma once
#include"ResourceManager.h"
#include<SFML\Graphics.hpp>
enum class TEXTURE
{
    PADDLE,
    BALL,
    HOOK_CANNON,
    HOOK,
    ROPE,
    SIZE_UP,
    BASIC_GUN,
    BASIC_GUN_POWERUP,
    BASIC_BULLET
};

typedef ResourceManager<sf::Texture, TEXTURE> TextureManager;