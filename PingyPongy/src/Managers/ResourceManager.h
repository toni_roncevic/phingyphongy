#pragma once
#include<map>
#include<iostream>
#include<memory>
template<class ResourceType, class ResourceID>
class ResourceManager
{
public:
    ResourceManager();
    void loadResource(std::string filename, ResourceID resourceID);
    ResourceType &getResource(ResourceID resourceID);

private:
    std::map<ResourceID, std::unique_ptr<ResourceType>> m_ResourceMap;
};

template<class ResourceType, class ResourceID>
inline ResourceManager<ResourceType, ResourceID>::ResourceManager() : m_ResourceMap()
{
}

template<class ResourceType, class ResourceID>
inline void ResourceManager<ResourceType, ResourceID>::loadResource(std::string filename, ResourceID resourceID)
{
#if _DEBUG
    std::cout << "Loading resource::" << filename << std::endl;
#endif
    std::unique_ptr<ResourceType> resource(new ResourceType());
    resource->loadFromFile(filename);
    m_ResourceMap.insert(std::pair<ResourceID, std::unique_ptr<ResourceType>>(resourceID, std::move(resource)));
}

template<class ResourceType, class ResourceID>
inline ResourceType & ResourceManager<ResourceType, ResourceID>::getResource(ResourceID resourceID)
{
    return *m_ResourceMap[resourceID];
}
