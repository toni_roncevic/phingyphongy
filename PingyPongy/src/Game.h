#pragma once
#include<iostream>
#include<SFML\Window.hpp>
#include<SFML\Graphics.hpp>
#include"Entities\EntityManager.h"
#include"Entities\Entity.h"
#include"Entities\Paddle\HookCannon.h"
#include"Entities\Paddle\Paddle.h"
#include"Entities\Paddle\Ball.h"
#include"Managers\TextureManager.h"
#include "Entities\Powerups\SizeUpPowerup.h"
#include "Entities\Powerups\BasicGunPowerup.h"
#include "Effects\Bloom.h"
#include <string>
class Game
{

private:
    Bloom m_BloomEffect;
    sf::RenderWindow m_RenderWindow;
    EntityManager m_EntityManager;
    TextureManager m_TextureManager;
    bool m_Quit;
    bool m_IsFullscreen;
private:
    void handleInput();
    void draw();
    void update(const float &deltaTime);
    void initSandboxLevel();
    void loadResources();
public:
    Game(const int &screenWidth, const  int &screenHeight);
    void run();
};