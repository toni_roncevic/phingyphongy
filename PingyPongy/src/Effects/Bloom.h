#pragma once
#include<SFML\Graphics.hpp>

class Bloom
{
private:
    sf::RenderTexture m_FirstPassTextures[2];
    sf::RenderTexture m_SecondPassTextures[2];
    sf::RenderTexture m_BrightnessTexture;
    sf::RenderTexture m_SceneTexture;
    sf::Texture m_SceneTex;
    //Shaders
    sf::Shader m_BrightnessShader;
    sf::Shader m_BlurShader;
    sf::Shader m_AddShader;
    sf::Shader m_DownsampleShader;

private:
    void applyBright(sf::RenderTexture &input, sf::RenderTexture &output);
    void applyDownsample(sf::RenderTexture &input, sf::RenderTexture &output);
    void applyBlur(sf::RenderTexture textures[2]);
    void blur(sf::RenderTexture &input, sf::RenderTexture &output, sf::Vector2f offset);
    void applyAdd(sf::RenderTexture &source, sf::RenderTexture &bloom, sf::RenderTarget &output);
    void createTextures(sf::Vector2u screenSize);
    void initShaders();
public:
    void apply(sf::RenderWindow &target);
    Bloom(sf::Vector2u screenSize);
};