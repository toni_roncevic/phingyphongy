#include "Bloom.h"

void Bloom::applyBright(sf::RenderTexture &input, sf::RenderTexture &output)
{
    sf::Sprite brightShaderSprite(input.getTexture());
    m_BrightnessShader.setParameter("source", input.getTexture());

    output.draw(brightShaderSprite, &m_BrightnessShader);
    output.display();
}

void Bloom::applyDownsample(sf::RenderTexture & input, sf::RenderTexture & output)
{
    m_DownsampleShader.setParameter("source", input.getTexture());
    m_DownsampleShader.setParameter("sourceSize", sf::Vector2f(input.getSize()));
    sf::Sprite downSampled(input.getTexture());
    output.draw(downSampled, &m_DownsampleShader);
    output.display();
}

void Bloom::applyBlur(sf::RenderTexture textures[2])
{
    sf::Vector2u textureSize = textures[0].getSize();
    for (int i = 0; i < 2; i++)
    {
        blur(textures[0], textures[1], sf::Vector2f(0.f, 1.f / textureSize.y));
        blur(textures[1], textures[0], sf::Vector2f(1.f / textureSize.x, 0.f));
    }
}

void Bloom::blur(sf::RenderTexture & input, sf::RenderTexture & output, sf::Vector2f offset)
{
    m_BlurShader.setParameter("source", input.getTexture());
    m_BlurShader.setParameter("offsetFactor", offset);
    sf::Sprite blurred(input.getTexture());

    output.draw(blurred, &m_BlurShader);
    output.display();
}

void Bloom::applyAdd(sf::RenderTexture & source, sf::RenderTexture & bloom, sf::RenderTarget & output)
{
    sf::Sprite added(source.getTexture());
    m_AddShader.setParameter("source", source.getTexture());
    m_AddShader.setParameter("bloom", bloom.getTexture());
    output.draw(added, &m_AddShader);
}

void Bloom::createTextures(sf::Vector2u screenSize)
{
    m_SceneTexture.create(screenSize.x, screenSize.y);
    m_SceneTexture.setSmooth(true);

    m_BrightnessTexture.create(screenSize.x, screenSize.y);
    m_BrightnessTexture.setSmooth(true);
    
    m_FirstPassTextures[0].create(screenSize.x , screenSize.y);
    m_FirstPassTextures[0].setSmooth(true);
    m_FirstPassTextures[1].create(screenSize.x , screenSize.y );
    m_FirstPassTextures[1].setSmooth(true);

    m_SecondPassTextures[0].create(screenSize.x , screenSize.y );
    m_SecondPassTextures[0].setSmooth(true);
    m_SecondPassTextures[1].create(screenSize.x , screenSize.y );
    m_SecondPassTextures[1].setSmooth(true);

    m_SceneTex.create(screenSize.x, screenSize.y);
    m_SceneTex.setSmooth(true);
}

void Bloom::initShaders()
{
    m_BrightnessShader.loadFromFile("Brightness.frag", sf::Shader::Fragment);
    m_BlurShader.loadFromFile("GuassianBlur.frag", sf::Shader::Fragment);
    m_AddShader.loadFromFile("Add.frag", sf::Shader::Fragment);
    m_DownsampleShader.loadFromFile("DownSample.frag", sf::Shader::Fragment);
}

void Bloom::apply(sf::RenderWindow & target)
{
    m_SceneTex.update(target);
    m_SceneTexture.display();
    sf::Sprite screen(m_SceneTex);
    m_SceneTexture.draw(screen);
    m_BrightnessTexture.clear();
    //applyBright(m_SceneTexture, m_BrightnessTexture);
    
    applyDownsample(m_SceneTexture, m_FirstPassTextures[0]);
    applyBlur(m_FirstPassTextures);
    

    applyDownsample(m_FirstPassTextures[0], m_SecondPassTextures[0]);
    applyBlur(m_SecondPassTextures);

    applyAdd(m_FirstPassTextures[0], m_SecondPassTextures[0], m_FirstPassTextures[1]);
    m_FirstPassTextures[1].display();

    applyAdd(m_SceneTexture, m_FirstPassTextures[1], target);
    target.display();
    
}

Bloom::Bloom(sf::Vector2u screenSize)
{
    createTextures(screenSize);
    initShaders();
}
