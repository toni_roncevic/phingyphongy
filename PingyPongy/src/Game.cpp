#include "Game.h"

Game::Game(const int &screenWidth, const int &screenHeight) : m_RenderWindow(sf::VideoMode(screenWidth, screenHeight), "Phingy Phongy"), m_Quit(false),
                                                            m_EntityManager(), m_BloomEffect(sf::Vector2u(screenWidth, screenHeight))
{
}

void Game::handleInput()
{
    sf::Event m_Event;
    if (m_RenderWindow.pollEvent(m_Event))
    {
        if (m_Event.type == sf::Event::Closed || (m_Event.type == sf::Event::KeyPressed && m_Event.key.code == sf::Keyboard::Escape))
            m_Quit = true;
        m_EntityManager.inputHandleEntities(m_Event);
    }
    if (m_Event.type == sf::Event::KeyPressed)
    {
        if (m_Event.key.code == sf::Keyboard::Num1)
        {
            std::cout << "Create Powerup" << std::endl;
            std::unique_ptr<SizeupPowerup> powerup(new SizeupPowerup(m_TextureManager));
            m_EntityManager.addEntity(std::move(powerup));
        }
        if (m_Event.key.code == sf::Keyboard::Num2)
        {
            std::cout << "Create Powerup" << std::endl;
            std::unique_ptr<BasicGunPowerup> powerup(new BasicGunPowerup(m_EntityManager, m_RenderWindow,m_TextureManager));
            m_EntityManager.addEntity(std::move(powerup));
        }
    }
}

void Game::draw()
{
    m_RenderWindow.clear(sf::Color(0,0,0));
    m_EntityManager.drawEntities(m_RenderWindow);
    m_BloomEffect.apply(m_RenderWindow);
    //m_RenderWindow.display();
}

void Game::update(const float &deltaTime)
{
    m_EntityManager.updateEntities(deltaTime);
}

void Game::initSandboxLevel()
{
    sf::Vector2i &winPos = m_RenderWindow.getPosition();

    std::unique_ptr<Ball> ball(new Ball(m_TextureManager));
    ball->setPosition(sf::Vector2f(500, 500));
    
    std::unique_ptr<Paddle> firstPlayer(new Paddle(m_TextureManager, *ball, m_EntityManager, m_RenderWindow));
    firstPlayer->setPosition(sf::Vector2f(100, 500));

    std::unique_ptr<Paddle> secondPlayer(new Paddle(m_TextureManager, *ball, m_EntityManager, m_RenderWindow));
    secondPlayer->setPosition(sf::Vector2f(900, 500));
    secondPlayer->setAIControlled(true);

    m_EntityManager.addEntity(std::move(firstPlayer));
    m_EntityManager.addEntity(std::move(secondPlayer));
    m_EntityManager.addEntity(std::move(ball));
}

void Game::loadResources()
{
    m_TextureManager.loadResource("paddle.png", TEXTURE::PADDLE);
    m_TextureManager.loadResource("ball.png", TEXTURE::BALL);
    m_TextureManager.loadResource("hook_cannon.png", TEXTURE::HOOK_CANNON);
    m_TextureManager.loadResource("hook.png", TEXTURE::HOOK);
    m_TextureManager.loadResource("rope.png", TEXTURE::ROPE);
    m_TextureManager.loadResource("size_up.png", TEXTURE::SIZE_UP);
    m_TextureManager.loadResource("basic_gun_powerup.png", TEXTURE::BASIC_GUN_POWERUP);
    m_TextureManager.loadResource("basic_gun.png", TEXTURE::BASIC_GUN);
    m_TextureManager.loadResource("basic_bullet.png", TEXTURE::BASIC_BULLET);
}

void Game::run()
{
    loadResources();
    initSandboxLevel();

    sf::Clock clock;
    sf::Time timeSinceLastUpdate = sf::Time::Zero;
    sf::Time timePerFrame = sf::seconds(1.0f / 60.0f);
    float lastTime = 0;
    sf::Clock fpsClock;
    while (!m_Quit)
    {
        float currentTime = fpsClock.restart().asSeconds();
        float FPS = 1.f / (currentTime - lastTime);
        lastTime = currentTime;
        std::string fpsString = std::to_string(FPS);
        m_RenderWindow.setTitle(fpsString);

        sf::Time elapsedTime = clock.restart();
        timeSinceLastUpdate += elapsedTime;
        while (timeSinceLastUpdate > timePerFrame)
        {
            timeSinceLastUpdate -= timePerFrame;
            handleInput();
            update(timePerFrame.asSeconds());
        }
        draw();

        
    }
}
