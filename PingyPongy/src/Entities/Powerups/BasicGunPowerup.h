#pragma once
#include "Powerup.h"
#include "..\Paddle\Paddle.h"
#include "BasicGun.h"
class BasicGunPowerup : public Powerup
{
public:
    void onPowerupPickup(Paddle *picker) override;
public:
    BasicGunPowerup(EntityManager &entityManager, sf::RenderWindow &renderWindow, TextureManager &textureManager);
private:
    sf::RenderWindow *m_RenderWindow;

private:
    TextureManager *m_TextureManager;
    EntityManager *m_EntityManager;
};
