#pragma once
#include"..\Entity.h"
#include"..\Paddle\Paddle.h"
enum class PowerupType
{
    SIZE_INCREASE
};

class Paddle;

class Powerup : public Entity
{   
public:
    PowerupType m_PowerupType;
    bool m_PickedUp;//Picked up by paddle
    Entity *m_AttachedEntity;
    sf::Sprite m_Sprite;
    float m_Speed;

private:
    int m_Direction;

private:
    void draw(sf::RenderWindow &renderWindow) override;
    void update(const float &deltaTime) override;

public:
    void attachTo(Entity *entity);
    virtual void onPowerupPickup(Paddle *picker);
    Powerup();
};