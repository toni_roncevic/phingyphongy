#include "BasicBullet.h"

void BasicBullet::update(const float & deltaTime)
{
    moveForward(deltaTime);
    m_Position = m_Sprite.getPosition();
    if (m_Position.x < 0 || m_Position.x > 1024 || m_Position.y < 0 || m_Position.y > 768)
        m_ShouldDestroy = true;
}

void BasicBullet::draw(sf::RenderWindow &renderWindow)
{
    renderWindow.draw(m_Sprite);
}

void BasicBullet::moveForward(const float &deltaTime)
{
    sf::Vector2f velocity;
    float radians = (3.14 / 180) * m_Rotation;
    velocity.y = std::sin(radians) * m_Speed * deltaTime;
    velocity.x = std::cos(radians) * m_Speed * deltaTime;
    m_Sprite.move(velocity);
}

BasicBullet::BasicBullet(TextureManager & textureManager, float &rotation, sf::Vector2f &startPosition) : m_Speed(1000)
{
    m_Rotation = rotation;
    m_Sprite.setPosition(startPosition);
    m_Sprite.setTexture(textureManager.getResource(TEXTURE::BASIC_BULLET));
}
