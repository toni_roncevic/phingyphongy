#include "SizeUpPowerup.h"

void SizeupPowerup::onPowerupPickup(Paddle * picker)
{
    //TODO: Limit this
    picker->scale(0.2f);
}

SizeupPowerup::SizeupPowerup(TextureManager & textureManager)
{
    m_PowerupType = PowerupType::SIZE_INCREASE;
    m_Sprite.setTexture(textureManager.getResource(TEXTURE::SIZE_UP));
    m_Size = sf::Vector2f(m_Sprite.getLocalBounds().width, m_Sprite.getLocalBounds().height);
}
