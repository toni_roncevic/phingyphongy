#pragma once
#include "..\Entity.h"
#include"..\Paddle\Paddle.h"
#include"..\..\Managers\TextureManager.h"
#include"..\..\Util\Util.h"
#include"BasicBullet.h"
class BasicGun : public Entity
{
private:
    void update(const float &deltaTime) override;
    void draw(sf::RenderWindow &renderWindow) override;
    void handleInput(sf::Event &windowEvents) override;
    void toPaddle();
    void fire();
private:
    sf::RenderWindow *m_RenderWindow;
    Paddle *m_Paddle;
    sf::Sprite m_Sprite;
    float m_Rotation;
    TextureManager *m_TextureManager;
    EntityManager *m_EntityManager;
public:
    BasicGun(TextureManager & textureManager,EntityManager &entityManager, sf::RenderWindow &renderWindow, Paddle *m_Paddle);
};