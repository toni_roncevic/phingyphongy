#include "BasicGunPowerup.h"

void BasicGunPowerup::onPowerupPickup(Paddle * picker)
{
    //Disable the hook cannon and replace it with gun "cannon"
    picker->disableHook();
    //Spawn gun
    
    std::unique_ptr<BasicGun> gun(new BasicGun(*m_TextureManager,*m_EntityManager, *m_RenderWindow,picker));
    m_EntityManager->addEntity(std::move(gun));
}

BasicGunPowerup::BasicGunPowerup(EntityManager & entityManager,sf::RenderWindow &renderWindow, TextureManager &textureManager)
{
    m_RenderWindow = &renderWindow;
    m_EntityManager = &entityManager;
    m_TextureManager = &textureManager;
    m_Sprite.setTexture(textureManager.getResource(TEXTURE::BASIC_GUN_POWERUP));
    m_Size = sf::Vector2f(m_Sprite.getLocalBounds().width, m_Sprite.getLocalBounds().height);
}
