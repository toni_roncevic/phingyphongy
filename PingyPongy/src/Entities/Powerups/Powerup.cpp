#include "Powerup.h"

void Powerup::onPowerupPickup(Paddle * picker)
{
    //Override this
}

void Powerup::draw(sf::RenderWindow & renderWindow)
{
    renderWindow.draw(m_Sprite); 
}

void Powerup::update(const float &deltaTime)
{
    if (m_AttachedEntity != nullptr)
    {
        m_Sprite.setPosition(m_AttachedEntity->m_Position);
    }
    else
    {
        sf::Vector2f currentPosition = m_Sprite.getPosition();
        if (currentPosition.y <= 0)
            m_Direction *= -1;
        else if (currentPosition.y >= 768)
            m_Direction *= -1;
        sf::Vector2f velocity = sf::Vector2f(0, m_Speed * m_Direction * deltaTime);
        m_Sprite.move(velocity);
    }
    m_Position = m_Sprite.getPosition();
}

void Powerup::attachTo(Entity * entity)
{
    m_AttachedEntity = entity;
}

Powerup::Powerup() : m_Speed(700), m_Direction(-1)
{
    m_EntityType = EntityType::POWER_UP;
    m_PickedUp = false;
    m_Sprite.setPosition(1024 / 2, 0);
    m_Position = m_Sprite.getPosition();
}
