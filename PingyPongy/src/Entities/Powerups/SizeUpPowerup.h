#pragma once
#include "Powerup.h"
#include"..\..\Managers\TextureManager.h"
class SizeupPowerup : public Powerup
{
private: 
    void onPowerupPickup(Paddle *picker) override;

public: 
    SizeupPowerup(TextureManager &textureManager);
};