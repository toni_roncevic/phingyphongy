#pragma once
#include "..\Entity.h"
#include "..\..\Managers\TextureManager.h"
class BasicBullet : public Entity
{
private:
    sf::Sprite m_Sprite;
    float m_Rotation;
    float m_Speed;
private:
    void update(const float &deltaTime) override;
    void draw(sf::RenderWindow &renderWindow)override;
    void moveForward(const float &deltaTime);
public:
    BasicBullet(TextureManager &textureManager, float &rotation, sf::Vector2f &startPosition);
};