#include "BasicGun.h"

void BasicGun::update(const float & deltaTime)
{
    //handle position
    toPaddle();
    //handle rotation
    sf::Vector2i windowPosition = m_RenderWindow->getPosition();
    sf::Vector2i &mousePositionGlobal = sf::Mouse::getPosition();
    sf::Vector2i mouseWindowPosition(mousePositionGlobal.x - windowPosition.x, mousePositionGlobal.y - windowPosition.y);
    m_Sprite.setRotation(Util::getPointToMouseAngle(*this, mouseWindowPosition));
    m_Position = m_Sprite.getPosition();
}

void BasicGun::draw(sf::RenderWindow & renderWindow)
{
    renderWindow.draw(m_Sprite);
}

void BasicGun::handleInput(sf::Event & windowEvents)
{
    if (windowEvents.type == sf::Event::MouseButtonPressed && windowEvents.key.code == sf::Mouse::Left)
    {
        fire();
    }
}

void BasicGun::toPaddle()
{
    sf::Vector2f destination = m_Paddle->m_Position;
    destination.y += m_Paddle->m_Size.y / 2;
    destination.x += 30;
    m_Sprite.setPosition(destination);
}

void BasicGun::fire()
{
    float rotation = m_Sprite.getRotation();
    sf::Vector2f startPos = m_Sprite.getPosition();
    //Create bullet
    std::unique_ptr<BasicBullet> bullet(new BasicBullet(*m_TextureManager, rotation, startPos));
    m_EntityManager->addEntity(std::move(bullet));
}

BasicGun::BasicGun(TextureManager & textureManager, EntityManager &entityManager, sf::RenderWindow &renderWindow, Paddle *paddle)
{
    m_EntityType = EntityType::BASIC_GUN;
    m_RenderWindow = &renderWindow;
    m_Paddle = paddle;
    m_TextureManager = &textureManager;
    m_EntityManager = &entityManager;
    m_Sprite.setTexture(textureManager.getResource(TEXTURE::BASIC_GUN));
    m_Size = sf::Vector2f(m_Sprite.getLocalBounds().width, m_Sprite.getLocalBounds().height);
    m_Sprite.setOrigin(0, m_Sprite.getLocalBounds().height / 2);
}
