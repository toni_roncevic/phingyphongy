#include"Entity.h"

Entity::Entity() : m_Position(0, 0)
{
}

void Entity::draw(sf::RenderWindow & renderWindow)
{
    //Override this
}

void Entity::update(const float & deltaTime)
{
    //Override
}

void Entity::onCollision(Entity & collidingEntity)
{
    //Override this
}

void Entity::handleInput(sf::Event & windowEvents)
{
    //override this
}

void Entity::move(sf::Vector2f &velocity)
{
    m_Position += velocity;
}
