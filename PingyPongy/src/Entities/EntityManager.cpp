#include "EntityManager.h"

EntityManager::EntityManager() : m_EntityList()
{
}

void EntityManager::drawEntities(sf::RenderWindow &renderWindow)
{
    for(auto &entity : m_EntityList)
    {
        entity->draw(renderWindow);
    }
}

void EntityManager::updateEntities(const float &deltaTime)
{
    for (auto &entity : m_EntityList)
    {


        entity->update(deltaTime);
        updateCollision(entity);

    }
    for (int i = 0; i < m_EntityList.size(); i++)
    {
        if (m_EntityList.at(i)->m_ShouldDestroy)
        {
            removeEntity(m_EntityList.at(i));
            i--;
        }
    }
    addNewEntites();
}

void EntityManager::inputHandleEntities(sf::Event &windowEvents)
{
    for (auto &entity : m_EntityList)
    {
        entity->handleInput(windowEvents);
    }
}

void EntityManager::addEntity(EntityPtr newEntity)
{
    m_Queue.push_back(std::move(newEntity));
}

void EntityManager::addNewEntites()
{
    while (m_Queue.size() > 0)
    {
        m_EntityList.push_back(std::move(m_Queue.at(0)));
        m_Queue.erase(m_Queue.begin());
    }
}

void EntityManager::removeEntity(EntityPtr &entity)
{
    m_EntityList.erase(std::remove(m_EntityList.begin(), m_EntityList.end(), entity));
    
}

void EntityManager::updateCollision(EntityPtr &callerEntity)
{
    for (auto &entity : m_EntityList)
    {
        if (entity != callerEntity)
        {
            checkForCollisions(callerEntity, entity);
        }
    }
}

void EntityManager::checkForCollisions(EntityPtr &entity, EntityPtr &target)
{
    if (entity->m_Position.x + entity->m_Size.x > target->m_Position.x && entity->m_Position.x < target->m_Position.x + target->m_Size.x &&
        entity->m_Position.y + entity->m_Size.y > target->m_Position.y && entity->m_Position.y < target->m_Position.y + target->m_Size.y)
        entity->onCollision(*target);
}
