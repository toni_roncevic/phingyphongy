#pragma once
#include<SFML\System.hpp>
#include<SFML\Graphics.hpp>
#include"EntityTypes.h"


class Entity
{
public:
    EntityType m_EntityType;
    sf::Vector2f m_Position;//X, y
    sf::Vector2f m_Size;//Width, height
    bool m_ShouldDestroy = false;
public:
    Entity();
    virtual void draw(sf::RenderWindow &renderWindow);
    virtual void update(const float &deltaTime);
    virtual void onCollision(Entity &collidingEntity);
    virtual void handleInput(sf::Event &windowEvents);
    virtual void move(sf::Vector2f &velocity);
};