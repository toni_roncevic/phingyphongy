#include "Hook.h"

void Hook::onCollision(Entity & collidingEntity)
{
    if (collidingEntity.m_EntityType == EntityType::HOOK_CANNON)
    {
        if (isReturning)
        {
            isFired = false;
            isReturning = false;
        }
    }
    if (collidingEntity.m_EntityType == EntityType::POWER_UP)
    {
        Powerup *powerupEntity = (Powerup*)(&collidingEntity);
        if (!hasPowerup && !powerupEntity->m_PickedUp)
        {
            std::cout << "Collision with powerup" << std::endl;
            //Attach powerup to hook
            Powerup *hookedPowerup = (Powerup*)&collidingEntity;
            hookedPowerup->attachTo(this);
            hasPowerup = true;
        }
    }
}

void Hook::draw(sf::RenderWindow & renderWindow)
{
    if (m_IsDisabled)
    {
        return;
    }
    renderWindow.draw(m_HookSprite);
    if (isReturning || isFired)
    {
        renderWindow.draw(m_Rope);
    }
}

void Hook::update(const float & deltaTime)
{
    if (m_IsDisabled)
    {
        return;
    }
    if (!isFired)
    {
        rotateAsCannon();
    }
    else
    {
        //Fire hook forwards
        if (!isReturning)
        {
            if (m_HookSprite.getPosition().x >= 1024 / 2)
            {
                isReturning = true;
            }
            else
            {
                moveHook(deltaTime);
            }
        }
        //Move hook backwards
        else
        {
            
            rotateTowardsCannon();
            
            moveHookBackwards(deltaTime);
        }
        updateRope();
    }
    m_Position = m_HookSprite.getPosition();
}

void Hook::fireHook()
{
    if (!isFired)
    {
        isFired = true;
        isReturning = false;
    }
}

void Hook::moveHook(const float & deltaTime)
{
    float hookRotation = m_HookSprite.getRotation() * (3.14f / 180);
    float velX = cos(hookRotation) * m_HookSpeed * deltaTime;
    float velY = sin(hookRotation) * m_HookSpeed * deltaTime;
    sf::Vector2f velocity = sf::Vector2f(velX, velY);

    m_HookSprite.move(velocity);

}

void Hook::moveHookBackwards(const float & deltaTime)
{
    float hookRotation = m_HookSprite.getRotation() * (3.14f / 180);
    float velX = cos(hookRotation) * m_HookSpeed * deltaTime *-1;
    float velY = sin(hookRotation) * m_HookSpeed * deltaTime * -1;
    sf::Vector2f velocity = sf::Vector2f(velX, velY);

    m_HookSprite.move(velocity);
}

void Hook::rotateAsCannon()
{
    //Update position based on cannon
    sf::Vector2f cannonPosition = m_HookCannon->m_CannonSprite.getPosition();

    float cx = cannonPosition.x;
    float cy = cannonPosition.y;
    float r = m_HookCannon->m_CannonSprite.getLocalBounds().width - 6;
    float t = m_HookCannon->m_CannonSprite.getRotation() * (3.14f / 180);

    float x = cx + r * std::cos(t);
    float y = cy + r * std::sin(t);

    sf::Vector2f finalPos = sf::Vector2f(x, y);

    m_HookSprite.setPosition(finalPos);
    m_HookSprite.setRotation(m_HookCannon->m_CannonSprite.getRotation());
}

void Hook::rotateTowardsCannon()
{
    m_Position = m_HookSprite.getPosition();
    sf::Vector2f cannonPosition = m_HookCannon->m_CannonSprite.getPosition();
    double PI = 3.14;
    int deltaX = (int)(cannonPosition.x + m_HookCannon->m_CannonSprite.getLocalBounds().width - 6) - (int)m_Position.x; //- (m_Size.y / 2);
    int deltaY = (int)m_HookCannon->m_CannonSprite.getPosition().y - (int)m_Position.y;// - m_Size.y * 2;

    double angleInDegrees = atan2(deltaY, deltaX) * (180 / PI);
    angleInDegrees += 180;
    m_HookSprite.setRotation((float)angleInDegrees);
}

void Hook::updateRope()
{
    //Update position based on cannon
    sf::Vector2f cannonPosition = m_HookCannon->m_CannonSprite.getPosition();

    float cx = cannonPosition.x;
    float cy = cannonPosition.y;
    float r = m_HookCannon->m_CannonSprite.getLocalBounds().width - 6;
    float t = m_HookCannon->m_CannonSprite.getRotation() * (3.14f / 180);

    float x = cx + r * std::cos(t);
    float y = cy + r * std::sin(t);

    sf::Vector2f ropeStartPos = sf::Vector2f(x, y);
    //Get distance between rope start and hook
    sf::Vector2f hookPos = m_HookSprite.getPosition();

    int dX = (hookPos.x - ropeStartPos.x) * (hookPos.x - ropeStartPos.x);
    int dY = (hookPos.y - ropeStartPos.y) * (hookPos.y - ropeStartPos.y);
    float dist = sqrt(dX + dY);

    float scaleX = dist / m_Rope.getLocalBounds().width;
    m_Rope.setScale(scaleX, 1);
    m_Rope.setPosition(ropeStartPos);

    //Rotate rope towards the hook
    double PI = 3.14;
    int deltaX = (int)m_Rope.getPosition().x - (int)hookPos.x;
    int deltaY = (int)m_Rope.getPosition().y - (int)hookPos.y;

    double angleInDegrees = atan2(deltaY, deltaX) * (180 / PI)+180;
    m_Rope.setRotation((float)angleInDegrees);
}

void Hook::detach(Powerup &powerup)
{
    hasPowerup = false;
    powerup.m_PickedUp = true;
    std::cout << "Detach powerup" << std::endl;
}

Hook::Hook(TextureManager & textureManager, HookCannon &cannon) : m_HookSprite(), isFired(false), hasPowerup(false),
    m_IsDisabled(false)
{
    m_HookCannon = &cannon;
    m_EntityType = EntityType::HOOK;
    m_Rope.setTexture(textureManager.getResource(TEXTURE::ROPE));
    m_HookSprite.setTexture(textureManager.getResource(TEXTURE::HOOK));
    m_HookSprite.setOrigin(0, m_HookSprite.getLocalBounds().height / 2);
    m_Size = sf::Vector2f(m_HookSprite.getLocalBounds().width, m_HookSprite.getLocalBounds().height);

}
