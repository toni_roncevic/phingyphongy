#include "HookCannon.h"

void HookCannon::draw(sf::RenderWindow & renderWindow)
{
    if (m_IsDisabled)
        return;
    renderWindow.draw(m_CannonSprite);
}

void HookCannon::update(const float & deltaTime)
{
    if (m_IsDisabled)
    {
        return;
    }
    //update the position of hook cannon so it is always at half of the paddle
    m_Position = m_Paddle->m_Position;
    m_Position.y = m_Position.y + m_Paddle->m_Size.y/2;
    m_Position.x = m_Position.x + 30;

    m_CannonSprite.setPosition(m_Position);

    //handle rotation
    sf::Vector2i &mousePositionGlobal = sf::Mouse::getPosition();
    sf::Vector2i mouseWindowPosition(mousePositionGlobal.x - m_WindowPosition.x, mousePositionGlobal.y - m_WindowPosition.y);
    m_CannonSprite.setRotation(Util::getPointToMouseAngle(*this, mouseWindowPosition));
}

void HookCannon::attachPaddle(Entity & paddleEntity)
{
    m_Paddle = (Paddle*)&paddleEntity;

}

void HookCannon::onCollision(Entity & collidingEntity)
{
    if (collidingEntity.m_EntityType == EntityType::POWER_UP)
    {
        Powerup *powerupEntity = (Powerup*)(&collidingEntity);
        m_Hook->detach(*powerupEntity);
        Powerup *pickedPowerup = (Powerup*)&collidingEntity;
        pickedPowerup->onPowerupPickup(m_Paddle);
        pickedPowerup->m_ShouldDestroy = true;
    }
}

void HookCannon::setHook(Hook *hook)
{
    m_Hook = hook;
}

HookCannon::HookCannon(TextureManager &textureManager, sf::RenderWindow &renderWindow, Entity &parentPaddle) : m_Angle(0), 
    m_WindowPosition(renderWindow.getPosition()),m_IsDisabled(false)
{
    m_CannonSprite.setTexture(textureManager.getResource(TEXTURE::HOOK_CANNON));
    m_Size = sf::Vector2f(m_CannonSprite.getLocalBounds().width, m_CannonSprite.getLocalBounds().height);
    m_CannonSprite.setOrigin(0, m_CannonSprite.getLocalBounds().height / 2);
    m_EntityType = EntityType::HOOK_CANNON;
    attachPaddle(parentPaddle);
}

void HookCannon::setPosition(sf::Vector2f & position)
{
    m_Position = position;
    m_CannonSprite.setPosition(position);
}
