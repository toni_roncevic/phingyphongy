#pragma once
#include"..\Entity.h"
#include"..\..\Managers\TextureManager.h"
#include<iostream>
#include<math.h>
enum class BallXDirection
{
    LEFT = -1,
    RIGHT = 1
};

enum class BallYDirection
{
    UP = -1,
    DOWN = 1
};

class Ball : public Entity
{
private:
    sf::Sprite m_BallSprite;
    sf::Vector2f m_Velocity;
    BallXDirection m_BallXDirection;
    BallYDirection m_BallYDirection;
    float m_MovementSpeed;
    Entity *m_lastCollidedEntity;

private:
    void restartBall();
    void draw(sf::RenderWindow &renderWindow) override;
    void update(const float &deltaTime) override;
    void movement(const float &deltaTime);
    void setTexture(sf::Texture &texture);
    void onCollision(Entity &collidingEntity) override;
    void calculateNewAngle(Entity &targetPaddle);

public:
    Ball(TextureManager &textureManager);
    void setPosition(sf::Vector2f &position);
};
