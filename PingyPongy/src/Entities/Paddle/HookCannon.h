#pragma once
#include"..\Entity.h"
#include<math.h>
#include<iostream>
#include"..\..\Managers\TextureManager.h"
#include"..\Powerups\Powerup.h"
#include"..\..\Util\Util.h"


class Paddle;
class Hook;
class HookCannon : public Entity
{
private:
    float m_Angle;
    sf::Vector2i m_WindowPosition;
    Paddle *m_Paddle;
    Hook *m_Hook;
    //sf::Vector2f *m_paddlePosition;
public:
    sf::Sprite m_CannonSprite;
    bool m_IsDisabled;
private:
    void draw(sf::RenderWindow &renderWindow) override;
    void update(const float &deltaTime) override;
    void attachPaddle(Entity &paddleEntity);
    void onCollision(Entity &collidingEntity) override;

public:
    HookCannon(TextureManager &textureManager, sf::RenderWindow &renderWindow, Entity &parentPaddle);
    void setPosition(sf::Vector2f &position);
    void setHook(Hook *hook);
};