#include "Paddle.h"

void Paddle::draw(sf::RenderWindow &renderWindow)
{
    renderWindow.draw(m_PaddleSprite);
}

void Paddle::update(const float &deltaTime)
{
    if (m_AIControlled)
        movementAI(deltaTime);
    else
        movement(deltaTime);
}

void Paddle::handleInput(sf::Event & windowEvents)
{
    //Check for key downs
    if (windowEvents.type == sf::Event::KeyPressed)
    {
        if (windowEvents.key.code == sf::Keyboard::W)
            m_UpPressed = true;
        if (windowEvents.key.code == sf::Keyboard::S)
            m_DownPressed = true;
    }
    //Check for key releases
    if (windowEvents.type == sf::Event::KeyReleased)
    {
        if (windowEvents.key.code == sf::Keyboard::W)
            m_UpPressed = false;
        if (windowEvents.key.code == sf::Keyboard::S)
            m_DownPressed = false;
    }

    if (windowEvents.type == sf::Event::MouseButtonPressed)
    {
        if (windowEvents.key.code == sf::Mouse::Button::Left)
        {
            m_Hook->fireHook();
        }
    }
}


void Paddle::movement(const float &deltaTime)
{
    sf::Vector2f velocity(0, 0);
    if (m_DownPressed)
        velocity.y += m_MovementSpeed * deltaTime;
    if (m_UpPressed)
        velocity.y -= m_MovementSpeed * deltaTime;
    m_PaddleSprite.setPosition(m_Position);
    move(velocity);
    checkIfOutOfScreen();
    
}

void Paddle::movementAI(const float & deltaTime)
{
    sf::Vector2f velocity(0, 0);
    float realMovSpeed = m_MovementSpeed * deltaTime;
    if (m_Position.y < m_Ball->m_Position.y + m_Ball->m_Size.y && m_Position.y + m_Size.y > m_Ball->m_Position.y)
    { }
    else
    {
        if (m_Position.y < m_Ball->m_Position.y)
            velocity.y += realMovSpeed;
        else if (m_Position.y > m_Ball->m_Position.y)
            velocity.y -= realMovSpeed;
    }
    move(velocity);
    checkIfOutOfScreen();
    m_PaddleSprite.setPosition(m_Position);
}

void Paddle::setTexture(sf::Texture & texture)
{
    m_PaddleSprite.setTexture(texture);
    m_Size.x = (float)texture.getSize().x;
    m_Size.y = (float)texture.getSize().y;
}

void Paddle::checkIfOutOfScreen()
{
    if (m_Position.y < 0)
        m_Position.y = 0;
    else if (m_Position.y + m_Size.y > 768)
        m_Position.y = 768 - m_Size.y;
}

void Paddle::setAIControlled(bool isAIControlled)
{
    m_AIControlled = isAIControlled;
}

void Paddle::scale(float pY)
{
    sf::Vector2f currentScale = m_PaddleSprite.getScale();
    currentScale.y = currentScale.y + (currentScale.y * pY);
    m_PaddleSprite.setScale(currentScale);
    m_Size = sf::Vector2f(m_PaddleSprite.getLocalBounds().width, m_PaddleSprite.getLocalBounds().height * currentScale.y);
}

void Paddle::disableHook()
{
    m_Hook->m_IsDisabled = true;
    m_Hook->m_HookCannon->m_IsDisabled = true;
}


void Paddle::enableHook()
{
    m_Hook->m_IsDisabled = false;
    m_Hook->m_HookCannon->m_IsDisabled = false;
}

void Paddle::setPosition(sf::Vector2f & position)
{
    m_PaddleSprite.setPosition(position);
    m_Position = position;
}

Paddle::Paddle(TextureManager &textureManager, Ball &ball, EntityManager &entityManager, sf::RenderWindow &renderWindow) : m_UpPressed(false), m_DownPressed(false), m_MovementSpeed(450), m_PaddleSprite()
{
    setTexture(textureManager.getResource(TEXTURE::PADDLE));
    m_EntityType = EntityType::PADDLE;
    m_Ball = &ball;

    //Instantiate the left cannon hook
    std::unique_ptr<HookCannon> leftCannon(new HookCannon(textureManager, renderWindow, *this));
    //Instantiate the left hook
    std::unique_ptr<Hook> leftHook(new Hook(textureManager, *leftCannon));
    leftCannon->setHook(&(*leftHook));
    m_Hook = &(*leftHook);

    entityManager.addEntity(std::move(leftCannon));
    entityManager.addEntity(std::move(leftHook));
}
