#pragma once
#include"..\Entity.h"
#include"..\EntityManager.h"
#include"Ball.h"
#include"HookCannon.h"
#include"Hook.h"
#include"..\..\Managers\TextureManager.h"

class Hook;

class Paddle : public Entity
{
private:
    float m_MovementSpeed;
    sf::Sprite m_PaddleSprite;
    bool m_UpPressed;
    bool m_DownPressed;
    bool m_AIControlled;
    Ball *m_Ball;
    Hook *m_Hook;
private:
    void draw(sf::RenderWindow &renderWindow) override;
    void update(const float &deltaTime) override;
    void handleInput(sf::Event &windowEvents) override;
    void movement(const float &deltaTime);
    void movementAI(const float &deltaTime);
    void setTexture(sf::Texture &texture);
    void checkIfOutOfScreen();
public:
    Paddle(TextureManager &textureManager, Ball &ball, EntityManager &entityManager, sf::RenderWindow &renderWindow);
    void setPosition(sf::Vector2f &position);
    void setAIControlled(bool isAIControlled);
    void scale(float pY);
    void disableHook();
    void enableHook();
};