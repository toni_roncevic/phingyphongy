#include "Ball.h"

void Ball::restartBall()
{
    m_MovementSpeed = 300.0f;
    m_Velocity.x = m_MovementSpeed;
    m_Velocity.y = 0;
    m_Position = sf::Vector2f(1024 / 2, 768 / 2);
    m_BallSprite.setPosition(m_Position);
    m_lastCollidedEntity = nullptr;
}

void Ball::draw(sf::RenderWindow & renderWindow)
{
    renderWindow.draw(m_BallSprite);
}

void Ball::update(const float & deltaTime)
{
    if (m_Position.x < 0 || m_Position.x + m_Size.x > 1024)
        restartBall();
    if (m_Position.y < 0 || m_Position.y + m_Size.y > 768)
        m_Velocity.y *= -1;
    movement(deltaTime);
}

void Ball::movement(const float &deltaTime)
{   
    m_BallSprite.move(m_Velocity * deltaTime);
    m_Position = m_BallSprite.getPosition();
}

void Ball::setTexture(sf::Texture & texture)
{
    m_BallSprite.setTexture(texture);
    m_Size.x = (float)texture.getSize().x;
    m_Size.y = (float)texture.getSize().y;
}

void Ball::onCollision(Entity & collidingEntity)
{
    if (collidingEntity.m_EntityType == EntityType::PADDLE)
    {
        if (m_lastCollidedEntity == &collidingEntity)
            return;

        calculateNewAngle(collidingEntity);
        m_MovementSpeed+=10;
        m_lastCollidedEntity = &collidingEntity;
    }
}

void Ball::calculateNewAngle(Entity &targetPaddle)
{
    //change the y velocity based on where it hit the paddle
    float t = ((m_Position.y - targetPaddle.m_Position.y) / targetPaddle.m_Size.y) - 0.5f;

    int curDir = m_Velocity.x < 0 ? 1 : -1;

    m_Velocity.x = m_MovementSpeed*curDir;
    m_Velocity.y = m_MovementSpeed*t;
}

Ball::Ball(TextureManager &textureManager) : m_BallSprite(), m_BallXDirection(BallXDirection::LEFT),m_BallYDirection(BallYDirection::UP), m_MovementSpeed(400)
{
    m_EntityType = EntityType::BALL;
    setTexture(textureManager.getResource(TEXTURE::BALL));
    m_Velocity = sf::Vector2f(m_MovementSpeed, m_MovementSpeed);
}

void Ball::setPosition(sf::Vector2f & position)
{
    m_BallSprite.setPosition(position);
    m_Position = position;
}
