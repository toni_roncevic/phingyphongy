#pragma once
#include "..\Entity.h"
#include"HookCannon.h"
#include"..\Powerups\Powerup.h"
#include "..\..\Managers\TextureManager.h"
#include<math.h>

class HookCannon;
class Powerup;
class Hook : public Entity
{
private:
    sf::Sprite m_HookSprite;
    sf::Sprite m_Rope;
    bool hasPowerup;
    bool isFired;
    bool isReturning;
    float m_HookSpeed = 1700;
private:
    void onCollision(Entity &collidingEntity) override;
    void draw(sf::RenderWindow &renderWindow) override;
    void update(const float &deltaTime) override;
    void moveHook(const float &deltaTime);
    void moveHookBackwards(const float &deltaTime);
    void rotateAsCannon();
    void rotateTowardsCannon();
    void updateRope();
public:
    HookCannon *m_HookCannon;
    bool m_IsDisabled;
public:
    Hook(TextureManager &textureManager, HookCannon &cannon);
    void fireHook();
    void detach(Powerup &powerup);
};