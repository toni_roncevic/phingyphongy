#pragma once
#include"Entity.h"
#include<vector>
#include<memory>
#include<iostream>

typedef std::unique_ptr<Entity> EntityPtr;

class EntityManager
{
private:
    std::vector<EntityPtr> m_EntityList;
    std::vector<EntityPtr> m_Queue;
public:
    EntityManager();
    void drawEntities(sf::RenderWindow &renderWindow);
    void updateEntities(const float &deltaTime);
    void inputHandleEntities(sf::Event &windowEvents);
    void addEntity(EntityPtr newEntity);
    void addNewEntites();
    void removeEntity(EntityPtr &entity);
    void updateCollision(EntityPtr &callerEntity);
    void checkForCollisions(EntityPtr &entity, EntityPtr &target);
};