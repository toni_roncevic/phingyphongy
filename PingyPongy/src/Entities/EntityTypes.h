#pragma once
enum class EntityType
{
    PADDLE,
    BALL,
    HOOK_CANNON,
    HOOK,
    POWER_UP,
    BASIC_GUN
};